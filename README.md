# Django Website IMAC

Website for student project !

# Init Project

Copy-Paste all files in your Project.
Run **npm install** to install all dependencies.

Create .babelrc file to enable es2015 and react and put the following line in it.
```
    {
        "presets": ["@babel/preset-env", "@babel/preset-react"]
    }
```

# Building
Create a dist Folder to allow webpack to put final files in it.

Run **npm run build** to build Project.

After many building run **npm run clear** and **npm run build** to delete unused main.js files.
# Development

Run **npm run dev** to launch a local server and start dev !