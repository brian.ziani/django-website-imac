import React from 'react'
import { AboutContainer, AboutWrapper,AboutTitle, AboutP, AboutSubTitle, AboutImg, AboutUL, AboutLI } from './about/aboutElements'
import { ContactTitle, ContactTitleWrapper } from './contact/contactElements';
import code_img from '../img/undraw_code.svg'
import abstract from '../img/undraw_abstract.svg'
import aboutme_img from '../img/undraw_aboutme.svg'
import Fade from 'react-reveal/Fade';
import { motion } from "framer-motion";
const About = () => {
    return (
        <motion.div exit={{ opacity: 0 }}>
        <Fade top>
            <ContactTitleWrapper>
                <ContactTitle>About</ContactTitle>
            </ContactTitleWrapper>
        </Fade>
        <AboutContainer>
            <Fade left delay={500}>
                <AboutWrapper>
                    <AboutImg src={code_img}/>
                </AboutWrapper>
            </Fade>
            <Fade right delay={800}>
                <AboutWrapper>
                    <AboutSubTitle>About Django</AboutSubTitle>
                    <AboutTitle>The Story of a Free Vjing Software</AboutTitle>
                    <AboutP>
                    The idea of Django emerged around February 2019. I was wondering how I could
                    get away from classic animation, where everything is mostly rigid. In Django, shapes are made of thousands of small points that each have their own motion.
                    </AboutP>
                    <AboutUL>
                        <AboutLI>Everything is Particles</AboutLI>
                        <AboutLI>Changing shape drags the particles into a new direction</AboutLI>
                        <AboutLI>Movement is governed by simple physical laws</AboutLI>
                    </AboutUL>
                </AboutWrapper>
            </Fade>
        </AboutContainer>
        <AboutContainer>
            <Fade left>
                <AboutWrapper>
                    <AboutSubTitle>About the Tech</AboutSubTitle>
                    <AboutTitle>Techs used by Django</AboutTitle>
                    <AboutP>
                        A performant implementation making use of your graphics card to speed up computations.
                    </AboutP>
                    <AboutUL>
                        <AboutLI>Written in C++</AboutLI>
                        <AboutLI>Using the OpenGL API</AboutLI>
                        <AboutLI>Leveraging the full power of GPGPU</AboutLI>
                    </AboutUL>
                </AboutWrapper>
            </Fade>
            <Fade right delay={500}>
                <AboutWrapper>
                    <AboutImg src={abstract}/>
                </AboutWrapper>
            </Fade>
        </AboutContainer>
        <AboutContainer>
            <Fade left delay={500}>
                <AboutWrapper>
                    <AboutImg src={aboutme_img}/>
                </AboutWrapper>
            </Fade>
            <Fade right delay={800}>
                <AboutWrapper>
                    <AboutSubTitle>About Me</AboutSubTitle>
                    <AboutTitle>Half an Artist, Half a Programmer</AboutTitle>
                    <AboutP>
                    At the time of the release of Django I am still a student (for a few months) at <a href="https://ingenieur-imac.fr/">IMAC</a>, an amazing school where I met a lot of fascinating people and discovered my passion for programming.
                    <br/>
                    Aside from studying I happen to work on some software and art projects. Django is the most developed one, but definitely not the only one.
                    </AboutP>
                    <AboutUL>
                        <AboutLI>Fond of Maths and Programming</AboutLI>
                        <AboutLI>Tries to make Art out of the two aforementioned passions</AboutLI>
                        <AboutLI>Loves Teaching</AboutLI>
                        <AboutLI>Check out <a href="https://julesfouchy.github.io/home">my personal website</a> !</AboutLI>
                    </AboutUL>
                </AboutWrapper>
            </Fade>
        </AboutContainer>
    </motion.div>
    )
}

export default About
