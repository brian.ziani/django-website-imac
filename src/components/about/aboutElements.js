import styled from 'styled-components'
import puce from '../../img/puce.svg'

export const AboutContainer  = styled.div`
    max-width: 1100px;
    position:relative;
    align-self:center;
    align-items:center;
    justify-content:space-evenly;
    margin:0 auto;
    margin-top:80px;
    margin-bottom:80px;
    display:flex;
    flex-flow:row nowrap;
    @media screen and (max-width:820px){
        flex-flow:column wrap;
        margin-top:0;
        margin-bottom:0;
    }
`

export const AboutWrapper = styled.div`
    width:100%;
    height:400px;
`


export const AboutTitle = styled.h2`
    font-size:32px;
    margin:0;
    padding: 10px;
`

export const AboutSubTitle = styled.p`
    text-align: left;
    font-size:20px;
    color:#0622D6;
    margin: 5px;
    padding: 10px;
`

export const AboutP = styled.p`
    text-align: justify;
    font-size:18px;
    padding: 10px;
    margin: 0;
`

export const AboutImg = styled.img`
    width: 100%;
    padding: 10px;
    &:hover {
        transition:1s all ease-in-out;
        transform:translateY(-5px);
    }
`
export const AboutUL = styled.ul`
    list-style: none;
    background-size:contain;
    padding:0;
    line-height: 1;
    font-size:18px;
    margin: 0;
    padding: 10px;
`
export const AboutLI = styled.li`
    padding: 10px;
    &:hover {
        &:before {
            transform: translateX(-20px);
            transition: 1s all ease;
        }
    }
    &:before {
        content: '';
        height: 15px;   
        width: 15px;    
        background-size: 15px;     
        display: inline-block;   
        background-image: url(${puce});
        margin-right: 10px;
        background-repeat: no-repeat;   
        background-position: center center;
        vertical-align: middle;
    }
`