import React, {useState} from 'react'
import { ContactContainer, ContactWrapper, ContactTitle, ContactInputWrapper, ContactLabel, ContactInput, ContactTextArea, ContactButton, ContactImgWrapper, ContactImg, ContactForm, ContactTitleWrapper, ContactFormTitle, ContactCardContainer, ContactCards, ContactCardTitle, ContactCardSvg, ContactCardP, ContactFormText } from './contact/contactElements';
import img from '../img/creative_experiment.svg'
import { Container } from './theme/theme';
import Fade from 'react-reveal/Fade';
import { motion } from "framer-motion";
import SocialMediaBanner from './socialMediaBanner'

const Contact = () => {
    const [status, setStatus] = useState("Submit");
    const handleSubmit = async (e) => {
      e.preventDefault();
      setStatus("Sending...");
      const { name, email, message,subject } = e.target.elements;
      let details = {
        name: name.value,
        email: email.value,
        message: message.value,
        subject: subject.value
      };
      let response = await fetch("https://django-website-server.herokuapp.com/contact", {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify(details),
      });
      setStatus("Submit");
      let result = await response.json();
      if(result.status === "success"){
        alert(result.message);

        this.setState(this.initialState());
      }
      else {
            alert(result.err);
            this.setState(this.initialState());
      }
    };

    return (
        <motion.div exit={{ opacity: 0 }}>
        <Fade top cascade>
            <ContactTitleWrapper>
                <ContactTitle>Contact Us</ContactTitle>
            </ContactTitleWrapper>
        </Fade>
        <ContactContainer>
            <Container>
                <ContactWrapper>
                    <ContactForm onSubmit={handleSubmit}>
                        <Fade left delay={500}>
                            <ContactFormTitle>A Question or something to say ?</ContactFormTitle>
                            <ContactFormText>Feel free to ask a question or make suggestions to help improving your experience of Django ! </ContactFormText>
                            <ContactInputWrapper>
                                <ContactLabel htmlFor='name'>Name</ContactLabel>
                                <ContactInput id='name' type='text' required/>
                            </ContactInputWrapper>
                            <ContactInputWrapper>
                                <ContactLabel htmlFor='email'>Email</ContactLabel>
                                <ContactInput id='email' type='email' required/>
                            </ContactInputWrapper>
                            <ContactInputWrapper>
                                <ContactLabel htmlFor='subject'>Subject</ContactLabel>
                                <ContactInput id='subject' type='text' required/>
                            </ContactInputWrapper>
                            <ContactInputWrapper>
                                <ContactLabel htmlFor='message'>Message</ContactLabel>
                                <ContactTextArea id='message' required/>
                            </ContactInputWrapper>
                            <ContactInputWrapper>
                                <ContactButton type='submit'>{status}</ContactButton>
                            </ContactInputWrapper>
                        </Fade>
                    </ContactForm>
                    <Fade right delay={1000}>
                        <ContactImgWrapper>
                            <ContactImg src={img}/>
                        </ContactImgWrapper>
                    </Fade>
                </ContactWrapper>
            </Container>
        </ContactContainer>
        <Fade top>
            <ContactContainer>
                <ContactFormTitle>Or simply get in touch on our Social Medias !</ContactFormTitle>
            </ContactContainer>
        </Fade>
        <ContactCardContainer>
            <SocialMediaBanner/>
        </ContactCardContainer>
        </motion.div>
    )
}

export default Contact
