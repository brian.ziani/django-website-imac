import styled,{ keyframes } from 'styled-components'

export const ContactContainer = styled.div`
    background: transparent;
    text-align: center;
    position: relative;
    margin-bottom:80px;
`

export const ContactWrapper = styled.div `
    padding: 30px;
    display: flex;
    margin-top:50px;
    justify-content: center;
    flex-flow:row nowrap;
    justify-content: center;
    @media screen and (max-width:820px){
        flex-flow:column wrap;
    }

`
export const ContactForm = styled.form`
    width: 100%;
`
export const ContactTitleWrapper = styled.div`
    height:200px;
    background: linear-gradient(0.25turn, #5B06D6, #1D04BF, #0655CC);
`
export const ContactTitle = styled.h1`
    font-size: 52px;
    padding: 50px;
    margin:0;
    color:black;
    z-index: 2;
    text-align: center;    
    color:white;
    font-family: 'YoungSerif', serif;
    @media screen and (max-width: 420px){
        font-size:42px;
    }
`
export const ContactFormTitle = styled.h2`
    font-size:32px;
    text-align: center;
    margin:0;
    padding: 10px;
`
export const ContactFormText = styled.h2`
    font-size:16px;
    text-align: left;
    margin:0;
    padding: 10px;
`
export const ContactLabel = styled.label`
    font-size:12px;
    color:grey;
    margin-bottom:5px;

`
export const ContactInputWrapper = styled.div`
    display: flex;
    flex-flow: row wrap;
    margin: 15px ;

`
export const ContactInput = styled.input`
    border-radius:3px;
    border:none;
    border:1px solid rgba(6, 85, 204, 0.5);
    height:30px;
    width: 100%;
    background-color:#F2F2F2;
    &:hover{
        border:1px solid rgba(6, 85, 204, 1);
        transition: 0.5s all ease-in-out;
    }
`
export const ContactTextArea = styled.textarea`
    height:150px;
    width: 100%;
    border:1px solid rgba(6, 85, 204, 0.5);
    border-radius:3px;
    outline:none;
    resize:none;
    overflow: auto;
    background-color:#F2F2F2;
    &:hover{
        border:1px solid rgba(6, 85, 204, 1);
        transition: 0.5s all ease-in-out;
    }
`

export const ContactButton = styled.button `
    border-radius: 6px;
    background: #F24C27;
    padding: 10px 22px;
    color:#fff;
    text-align: center;
    border: none;
    &:hover {
        transition: all 0.2s ease-in-out;
        background:#fff;
        color:#D94848;
        cursor:pointer;
        text-decoration:none;
        width:inherit;
    }
`
export const ContactImgWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    @media screen and (max-width:820px){
        display: none;
    }

`
export const ContactImg = styled.img`
    width: 100%;
`
export const ContactCardContainer = styled.section`
    display: flex;
    justify-content: space-evenly;
    padding: 20px;
    flex-flow:row wrap;
    margin-bottom:80px;
    @media screen and (max-width:420px){
        flex-flow:column wrap;
        height: 100%;
    }
`
export const ContactCards = styled.a`
    background:#fff;
    text-decoration: none;
    align-self:center;
    border-radius:5px;
    box-shadow:5px 5px 30px -5px rgba(0,0,0,0.65) ;
    display: flex;
    width: ${props => props.width};
    height: ${props => props.height};
    flex-flow:column nowrap;
    justify-content: flex-start;
    text-align: center;
    color:#0655CC;
    margin:10px;
    &:hover{
        color:#F24C27;
        transform:translateY(-5px);
        transition: 0.3s all ease-in-out;
    }
`
export const ContactCardSvg = styled.div`
    font-size:72px;
    padding:10px;
`

export const ContactCardTitle = styled.h3`
    font-size:22px;
    margin:0;
    color:black;
`
export const ContactCardP = styled.p`
    font-size:16px;
    text-align: ${props => props.textAlign};
    padding:10px;
`
export const ContactCardImg = styled.img`
        width:100%;
        height:100%;
        z-index:10;
        border-radius:3px;
`
export const ContactHoverable = styled.div`
    width:300px;
    height:300px;
    background:rgba(255,255,255,0.75);
    z-index:10;
    position: absolute;
    display: flex;
    border-radius:3px;
    justify-content: center;
    flex-direction: column;
    transition: 0.5s all ease-in-out;
`
