import React from 'react'
import { DonationWrapper, DownloadBtnWrapper, DownloadContainer, DownloadImg, DownloadText, DownloadTitle, DownloadWrapper,ExternalLink } from './download/donwloadElements'
import bgImg from '../img/interraction.gif'
import { HeroBtnLink } from './home/heroElements';
import { motion } from "framer-motion";

function Download() {
    const downloadLink = (() => {
        if ((navigator.appVersion.indexOf("Win") != -1) || (navigator.userAgent.indexOf("Win") != -1))
            return '/public/Django 1.1.0 Win64.zip'
        else if ((navigator.appVersion.indexOf("Linux") != -1) || (navigator.userAgent.indexOf("Linux") != -1))
            return '/public/Django 1.1.0 Linux64.tar.gz'
        else
            return 'https://github.com/JulesFouchy/Django/releases/tag/v1.1.0'
    })()
    return (
        <motion.div exit={{ opacity: 0 }}>
            <DownloadContainer>
                <DownloadImg src={bgImg}/>
                <DownloadWrapper>
                    <DownloadTitle>Download Now !</DownloadTitle>
                    <DownloadText fontSize ='20px' fontStyle='non'>Download our free VJing software or take a direct look at the GitHub page !</DownloadText>
                    <DownloadBtnWrapper>
                        <HeroBtnLink href={downloadLink} target='__blank' download>Download</HeroBtnLink>
                        <ExternalLink href='https://github.com/JulesFouchy/Django' target='__blank'>GitHub</ExternalLink>
                    </DownloadBtnWrapper>
                </DownloadWrapper>
                <DonationWrapper>
                    <DownloadText fontSize ='16px' fontStyle='italic'>To support Django's Developer, feel free to donate !</DownloadText>
                    <ExternalLink href="http://paypal.me/djangovjing" target='__blank'>Donate</ExternalLink>
                </DonationWrapper>
            </DownloadContainer>
        </motion.div>
    )
}

export default Download
