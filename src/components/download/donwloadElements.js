import styled from 'styled-components'
import {Link} from 'react-router-dom'
import bgImg from '../../img/interraction.gif'

export const DownloadContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-flow:row wrap;
    position: relative;
    height: 900px;
    width: 100%;
    &:nth-child(1){
        position: absolute;
        z-index: 1;
    }
    &:nth-child(2){
        position: absolute;
        z-index: 1;
    }
    @media screen and (max-width:820px){
        height:600px;
    }
`

export const DownloadImg = styled.img`
    height: 100%;
    width:100%;
`

export const DownloadWrapper = styled.div `
    padding: 20px;
    position: absolute;
    text-align:center;
`

export const DownloadTitle = styled.h1`
    font-size: 52px;
    color:white;
    z-index: 2;
    font-family: 'YoungSerif', serif;
    @media screen and (max-width: 420px){
        font-size:42px;
    }
`

export const DownloadText = styled.p`
    font-size: ${props => props.fontSize};
    font-style: ${props =>props.fontStyle};
    color: white;
    @media screen and (max-width: 420px){
        font-size:16px;
    }
`
export const DownloadBtnWrapper = styled.div`
    display: flex;
    flex-flow:row wrap;
    justify-content: space-evenly;
    align-items: center;
`

export const ExternalLink = styled.a`
    border-radius: 6px;
    background: #F24C27;
    padding: 10px 22px;
    color:#fff;
    display: flex;
    font-size:16px;
    width:fit-content;
    border:none;
    outline:none;
    cursor: pointer;
    align-items: center;
    transition: all 0.2s ease-in-out;
    text-decoration: none;
    margin:0 10px;
    &:hover {
        transition: all 0.2s ease-in-out;
        background:#fff;
        color:#D94848;
        text-decoration:none;
    }
    @media screen and (max-width: 420px){
        font-size:13px;
    }
`
export const DonationWrapper = styled.div`
    padding: 20px;
    text-align:justify;
    position:absolute;
    bottom:0;
    left:0;
`