import React from 'react'
import {FooterContainer, FooterWrap, FooterLink, FooterLinksContainer, FooterLinksItem, FooterLinksWrapper, FooterLinkTitle, SocialMedia, SocialMediaWrap, SocialLogo, WebsiteRights, SocialIcons, SocialIconLink } from './footerElements'
import {FaFacebook, FaInstagram, FaYoutube} from 'react-icons/fa'
import {SiTiktok} from 'react-icons/si'
const Footer = () => {
    return (
        <FooterContainer>
            <FooterWrap>
                <FooterLinksContainer>
                    <FooterLinksWrapper>
                        <FooterLinksItem>
                            <FooterLinkTitle>Useful Links</FooterLinkTitle>
                            <FooterLink to={'/'}>Home</FooterLink>
                            <FooterLink to={'/about'}>About</FooterLink>
                            <FooterLink to={'/download'}>Download</FooterLink>
                            <FooterLink to={'/contact'}>Contact</FooterLink>
                        </FooterLinksItem>
                    </FooterLinksWrapper>
                </FooterLinksContainer>
                <SocialMedia>
                    <SocialMediaWrap>
                        <SocialLogo to={'/'}>Django</SocialLogo>
                        <WebsiteRights>Django Software &#169; {new Date().getFullYear()} All Rights Reserved - <FooterLink to={'/legals'} title="Legal notices of Django Free VJing Software and its website">Legal Notices</FooterLink></WebsiteRights>
                        <SocialIcons>
                            <SocialIconLink href='https://www.facebook.com/django.vj' target='__blank' aria-label='Facebook'>
                                <FaFacebook />
                            </SocialIconLink>
                            <SocialIconLink href='https://www.tiktok.com/@django.vj' target='__blank' aria-label='Twitter'>
                                <SiTiktok />
                            </SocialIconLink>
                            <SocialIconLink href='https://www.instagram.com/django.vj' target='__blank' aria-label='Instagram'>
                                <FaInstagram />
                            </SocialIconLink>
                            <SocialIconLink href='https://www.youtube.com/channel/UCdGMTVCx1yfojoPKFGN2SDQ?fbclid=IwAR3UxYDM9PtZOs8xVSiXuSe9o1srduE4fo82ZVnVczu2_IXiaT0XUY8aV6Q' target='__blank' aria-label='Youtube'>
                                <FaYoutube />
                            </SocialIconLink>
                        </SocialIcons>
                    </SocialMediaWrap>
                </SocialMedia>
            </FooterWrap>
        </FooterContainer>
    )
}

export default Footer 