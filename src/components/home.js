import React, { useState } from 'react'
import Hero from './home/hero'
import { Container } from './theme/theme'
import Features from './home/features'
import { AboutContainer, AboutWrapper,AboutTitle, AboutP, AboutSubTitle, AboutImg, AboutUL, AboutLI } from './about/aboutElements'
import Video from './home/video'
import LazyLoad from 'react-lazyload';
import Fade from 'react-reveal/Fade';
import abstract from '../img/undraw_abstract.svg'
import { motion } from "framer-motion"
import { ContactCardImg, ContactCardP, ContactCards, ContactCardTitle, ContactHoverable, ContactCardContainer } from './contact/contactElements'
import SocialMediaBanner from './socialMediaBanner'
import julesImg from '../img/jules.jpg'
import cloeImg from '../img/cloe.jpg'

const Credit = (props) => (
    <ContactCards href={props.credit.link} target='__blank' height='300px' width="300px" onMouseEnter={() => props.setHoverFn(true)} onMouseLeave={() => props.setHoverFn(false)}>
        <ContactCardImg src={props.credit.imageSrc}></ContactCardImg>
        {props.isHovered &&                 
        <ContactHoverable>
            <ContactCardTitle>{props.credit.name}</ContactCardTitle>
            <ContactCardP textAlign='center'>{props.credit.title}</ContactCardP>
        </ContactHoverable>
        }
    </ContactCards>
)

const shuffle = a => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

const credits = shuffle([
    {
        link: 'https://julesfouchy.github.io/home',
        imageSrc: julesImg,
        name: 'Jules Fouchy',
        title: 'Developer of Django',
    },
    {
        link: 'https://www.linkedin.com/in/brian-ziani-7340a6154/',
        imageSrc: 'https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-9/44101693_2301967096745467_4898358627120709632_o.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_ohc=P9ztriADrZYAX_ta-eN&_nc_ht=scontent-cdt1-1.xx&oh=3cdf6efe61d19888c9723e242298b500&oe=60389ACA',
        name: 'Brian Ziani',
        title: 'Web Development',
    },
    {
        link: 'https://www.linkedin.com/in/th%C3%A9o-godard-034a0a15b/',
        imageSrc: 'https://media-exp1.licdn.com/dms/image/C4D03AQHfG8tas2R6JQ/profile-displayphoto-shrink_800_800/0/1520874082482?e=1617235200&v=beta&t=ycitCwJF6-vt88vE4FRp9bt6jVFq8_KTo1ibsLhMnCo',
        name: 'Théo Godard',
        title: 'Communication & Video Editor',
    },
    {
        link: '',
        imageSrc: cloeImg,
        name: 'Cloé Quirin',
        title: 'Video & Design',
    },
    {
        link: 'https://www.linkedin.com/in/am%C3%A9lie-crouigneau-4b6b35137/',
        imageSrc: 'https://media-exp1.licdn.com/dms/image/C4D03AQE6d7vjN80owQ/profile-displayphoto-shrink_800_800/0/1541958149142?e=1617235200&v=beta&t=HTSRKunV-I6YDl6gnCZX6phRM_TUuGTTujGcaewm2sA',
        name: 'Amélie Crouigneau',
        title: 'Translation & Web Content',
    },
    {
        link: 'https://www.linkedin.com/in/sylvie-zerbit-28253155/',
        imageSrc: 'https://media-exp1.licdn.com/dms/image/C5103AQHnZf7GmhP4dQ/profile-displayphoto-shrink_800_800/0/1516860594095?e=1617235200&v=beta&t=uC6osn34W1KRuOGPpb_BtYX_2Rjp8050cfV2UXKa_X0',
        name: 'Sylvie Zerbit',
        title: 'Teacher at Gustave Eiffel University & Supervisor of the communication',
    },
])

const Home = () => {
    const [inHover, setHover] = useState(false);
    const [inHover2, setHover2] = useState(false);
    const [inHover3, setHover3] = useState(false);
    const [inHover4, setHover4] = useState(false);
    const [inHover5, setHover5] = useState(false);
    const [inHover6, setHover6] = useState(false);

    return (
        <motion.div exit={{ opacity: 0 }}>
            <Hero />
            <Container>
                <Features />
            </Container>
            <LazyLoad>
                    <Video/>
            </LazyLoad>
            
        <AboutContainer>
            <AboutTitle>Find us on our Social Medias !</AboutTitle>
        </AboutContainer>
        <ContactCardContainer>
            <SocialMediaBanner/>
        </ContactCardContainer>
        <AboutContainer>
            <AboutTitle>The Amazing Team !</AboutTitle>
        </AboutContainer>
        <AboutContainer>
            <Credit credit={credits[0]} setHoverFn={setHover}  isHovered={inHover} />
            <Credit credit={credits[1]} setHoverFn={setHover2} isHovered={inHover2} />
            <Credit credit={credits[2]} setHoverFn={setHover3} isHovered={inHover3} />
        </AboutContainer>
        <AboutContainer>
            <Credit credit={credits[3]} setHoverFn={setHover4} isHovered={inHover4} />
            <Credit credit={credits[4]} setHoverFn={setHover5} isHovered={inHover5} />
            <Credit credit={credits[5]} setHoverFn={setHover6} isHovered={inHover6} />
        </AboutContainer>
        </motion.div> 
        
    )
}

export default Home
