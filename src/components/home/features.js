import React from 'react'
import { Cards, CardsSvg, CardsText, CardsTitle, FeaturesWrapper, FeatureTitle, TitleWrapper } from './featuresElements'
import {FaPalette, FaRegArrowAltCircleDown, FaShapes, FaDraftingCompass} from 'react-icons/fa'
import Fade from 'react-reveal/Fade';

const Features = () => {
    return (
        <FeaturesWrapper>
            <TitleWrapper>
                <FeatureTitle>Features</FeatureTitle>
            </TitleWrapper>
            <Fade left>
                <Cards>
                    <CardsTitle>Control Physics</CardsTitle>
                    <CardsSvg id="Physics"><FaDraftingCompass/></CardsSvg>
                    <CardsText>Set a unique mood for your performance by adjusting the behaviour of the particles.
                        <br/>
                        Choose from a variety of presets to get energic animations or relaxing ones.</CardsText>
                </Cards>
            </Fade>
            <Fade right>
                <Cards>
                    <CardsTitle>Play with Shapes</CardsTitle>
                    <CardsSvg id="Shapes"><FaShapes/></CardsSvg>
                    <CardsText>Discover the many default shapes and create or import you own.
                        <br/>
                        Transition between them with a single key press to create stunning animations.
                    </CardsText>
                </Cards>
            </Fade>
            <Fade left>
                <Cards>
                    <CardsTitle>Change Colors as you wish</CardsTitle>
                    <CardsSvg id="Colors"><FaPalette/></CardsSvg>
                    <CardsText>
                        Whether you prefer using the whole chromatic circle or a simple gradient between two nice colors, Django let's you set the tone you want for your sets.
                    </CardsText>
                </Cards>
            </Fade>
            <Fade right>
                <Cards>
                    <CardsTitle>Import and Share</CardsTitle>
                    <CardsSvg id="Import"><FaRegArrowAltCircleDown/></CardsSvg>
                    <CardsText>
                        Import shapes from any SVG or write your own custom layout !
                        <br/>
                        Grab some nice setups from your friends or share your own creations with the world !
                    </CardsText>
                </Cards>
            </Fade>
        </FeaturesWrapper>

    )
}

export default Features
