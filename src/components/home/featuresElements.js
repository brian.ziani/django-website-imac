import styled from 'styled-components'

export const TitleWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content:center;
    align-items:center;
    width: 100%;
`

export const FeatureTitle = styled.h1`
    font-size: 42px;

`

export const FeaturesWrapper = styled.section`
    position: relative;
    display: flex;
    flex-flow:row wrap;
    align-items:center;
    justify-content:space-evenly;
    margin: 14px 0;
    overflow: hidden;
`
export const Cards = styled.div`
    position: relative;
    width: 450px;
    box-shadow:2px 2px 10px grey;
    border-radius:3px;
    background:#fff;
    margin: 14px;
    display:grid;
    grid-template-columns: 20% auto;
    grid-template-rows: 20%;
    padding:10px;
    color:#0655CC;
    @media screen and (max-width:420px){
        width:300px;
    }
    &:hover{
        color:#F24C27;
        transform:translateY(-5px);
        transition: all 0.5s ease-in-out;
    }
`

export const CardsTitle = styled.h2`
    font-size:20px;
    margin:0;
    color: black;
    text-align:justify;
    grid-column-start: 2;
    grid-row-start: 1;
    @media screen and (max-width:420px){
        font-size:18px;
    }
`
export const CardsText = styled.p`
    color: grey;
    text-align:justify;
    margin-top:12px;
    font-size:16px;
    grid-column-start: 2;
    grid-row-start: 2;
    @media screen and (max-width:420px){
        font-size:14px;
    }
`
export const CardsSvg = styled.div`
    float: left;
    margin: 14px;
    font-size:52px;
    align-items:center;
    grid-column-start: 1;
    grid-row-start: 2;
    @media screen and (max-width:420px){
        font-size:42px;
    }
    &:hover{
        color:#F24C27;
        transition: all 0.5s ease-in-out;
    }
`