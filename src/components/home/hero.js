import React from 'react'
import { ButtonWrapper, HeroContainer, HeroDescription, HeroImg, HeroSoftware, HeroTitle, HeroWrapper, TextWrapper, HeroBtnLink, AngleRight, InstaLink} from './heroElements'
import img from '../../img/accueil.gif'
import Fade from 'react-reveal/Fade';
const Hero = () => {
    return (
        <Fade top>
                <HeroContainer>
                    <HeroWrapper>
                        <TextWrapper>
                            <HeroSoftware>Django, the Free VJing Software !</HeroSoftware>
                            <HeroTitle>Meet Django !
                            <iframe src="https://ghbtns.com/github-btn.html?user=JulesFouchy&repo=Django&type=star&count=true&size=large" frameBorder="0" scrolling="0" width="170" height="30" title="GitHub"></iframe>
                            </HeroTitle>
                            <HeroDescription>Django is a free VJing Software based on a particle system. It allows you to draw, import and manipulate shapes with ease.</HeroDescription>
                            <ButtonWrapper>
                                <HeroBtnLink to={'/download'}>Download <AngleRight /></HeroBtnLink>
                            </ButtonWrapper>
                            <InstaLink href='https://www.instagram.com/django.vj' target='__blank' aria-label='Instagram' >
                                #createwithdjango
                            </InstaLink>
                        </TextWrapper>
                    </HeroWrapper>
                    <HeroWrapper>
                        <HeroImg src={img} />
                    </HeroWrapper>
            </HeroContainer>
        </Fade>
    )
}

export default Hero
