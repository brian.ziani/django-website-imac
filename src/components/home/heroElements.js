import styled from 'styled-components'
import {Link} from 'react-router-dom'
import {FaAngleRight} from 'react-icons/fa'

export const HeroContainer = styled.div`
    background: '#fff';
    position: relative;
    display: flex;
    top: 0;
    left: 0;
    margin: 0 auto;
    flex-flow: row nowrap;
    margin-top: -130px;
    height: 750px;
    
    background:linear-gradient(0.25turn, #5B06D6, #1D04BF, #0655CC);
    transform: skewY(-6deg);
    transform-origin: top left;
    @media screen and (max-width:820px){
        flex-flow: column wrap;
        color: white;
        transform: none;
        margin-top: -80px;
        background:transparent;
    }

`

export const HeroWrapper = styled.div`
    transform: skewY(6deg);
    margin: 0 auto;
    height: 900px;
    display: flex;
    align-items: flex-start;
    flex-flow: column wrap;
    justify-content: center;
    text-align: left;
    width: 100%;
    z-index: 5;
    @media screen and (max-width:820px){
        height:600px;
        &:nth-child(2){
        position: absolute;
        z-index: 1;
        }
        transform: none;
    }
`
export const TextWrapper = styled.div `
    padding: 30px;
`
export const HeroSoftware = styled.p`
    color: #fff;
    font-size:14px;
    @media screen and (max-width:820px){
        color:white;
    }
`

export const HeroTitle = styled.h1`
    font-size: 52px;
    margin-right:20px;
    font-family: 'YoungSerif', serif;
    @media screen and (max-width: 420px){
        font-size:42px;
    }
`

export const HeroDescription = styled.p`
    font-size:20px;
    color: white;
    @media screen and (max-width: 420px){
        font-size:16px;
    }
`

export const HeroImg = styled.img`
    height: 500px;
    width:500px;
    border-radius: 50%;
    align-self:center;
    margin-top:300px;
    box-shadow:5px 5px 20px;
    @media screen and (max-width:820px){
        width:100%;
        height: 100%;
        margin:0;
        border-top-right-radius:0;
        border-top-left-radius:0;
        border-bottom-left-radius: 100% 30px;
        border-bottom-right-radius: 100% 30px;    
    }
`

export const ButtonWrapper = styled.div`
    padding: 20px;
    display:flex;
    justify-content:flex-start;
`

export const HeroBtnLink = styled.a`
    border-radius: 6px;
    background: #F24C27;
    padding: 10px 22px;
    color:#fff;
    display: flex;
    font-size:16px;
    border:none;
    outline:none;
    cursor: pointer;
    align-items: center;
    transition: all 0.2s ease-in-out;
    text-decoration: none;
    margin:0 10px;
    &:hover {
        transition: all 0.2s ease-in-out;
        background:#fff;
        color:#D94848;
        text-decoration:none;
    }
    @media screen and (max-width: 420px){
        font-size:13px;
    }
`
export const AngleRight = styled(FaAngleRight)`
    align-self:center;
`

export const InstaLink = styled.a`
    font-size:22px;
    text-decoration:none;
    color:#fff;
    padding: 10px;
`