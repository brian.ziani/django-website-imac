import React from 'react'
import { VideoWrapper, TitleWrapper,  VideoTitle, VideoCardsTitle, VideoCardsText, VideoCards, VideoIframe} from './videoElements'
import Fade from 'react-reveal/Fade';
const Video = () => {
    return (
        <>
        <TitleWrapper>
            <VideoTitle>Watch our Trailer Video</VideoTitle>
        </TitleWrapper>
        <VideoWrapper>
            <Fade top>
                <VideoCards>
                    <VideoIframe src="https://www.youtube.com/embed/ILEdhbt9lL0"
                    frameBorder="0" 
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                    allowfullscreen>
                    </VideoIframe>
                </VideoCards>
            </Fade>
        </VideoWrapper>
        </>
    )
}

export default Video
