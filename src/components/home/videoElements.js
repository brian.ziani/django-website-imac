import styled from 'styled-components'

export const TitleWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content:center;
    align-items:center;
    text-align: center;
    width: 100%;
    z-index: 999;
`

export const VideoTitle = styled.h1`
    font-size: 42px;
    
`

export const VideoWrapper = styled.section`
    display: flex;
    flex-flow:row wrap;
    align-items:center;
    height: 400px;
    z-index: 1;
    justify-content:center;
    margin: 14px 0;
    overflow: hidden;
    background:linear-gradient(0.25turn, #5B06D6, #1D04BF, #0655CC);
    transform-origin: top left;
`
export const VideoCards = styled.div`
    position: relative;
    width: 500px;
    background:transparent;
    @media screen and (max-width:420px){
        width:300px;
    }
`

export const VideoCardsTitle = styled.h2`
    font-size:20px;
    margin:0;
    color: black;
    text-align:center;
    grid-column-start: 2;
    grid-row-start: 1;
    @media screen and (max-width:420px){
        font-size:18px;
    }
`
export const VideoCardsText = styled.p`
    color: grey;
    text-align:justify;
    margin:12px;
    font-size:16px;
    z-index: 989;
    grid-column-start: 2;
    grid-row-start: 2;
    @media screen and (max-width:420px){
        font-size:14px;
    }
`

export const VideoIframe = styled.iframe`
    width: 100%;
    height: 315px;
    border-radius:3px;

`