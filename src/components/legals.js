import React from 'react'
import styled from 'styled-components'
import { motion } from "framer-motion"
import Fade from 'react-reveal/Fade';
import { ContactTitle, ContactTitleWrapper } from './contact/contactElements';

const LegalsWrapper = styled.div`
    display: flex;
    flex-flow:column wrap;
    position: relative;
    padding: 22px;
`
const LegalTitle = styled.h2`
    font-size:22px;
`

const LegalText = styled.p`
    font-size:16px;
`
const LegalLink = styled.a`
    color: #0655CC;
    text-decoration:none;
    &:hover{
        transition:0.5s all ease-in-out;
        color: #F24C27;
    }
`
const Legals = () => {
    return (
        <motion.div exit={{ opacity: 0 }}>
            <Fade top cascade>
                <ContactTitleWrapper>
                    <ContactTitle>Legal Notices</ContactTitle>
                </ContactTitleWrapper>
                <LegalsWrapper>
                    <LegalTitle>Who is in  charge of this website ?</LegalTitle>
                    <LegalText>Mister <LegalLink href='https://github.com/JulesFouchy/' target='__blank' title='Link to Jules Fouchy Github'>Jules Fouchy</LegalLink></LegalText>
                </LegalsWrapper>
                <LegalsWrapper>
                    <LegalTitle>Who Developed this website ?</LegalTitle>
                    <LegalText>Student at the IMAC Engineering School,  
                        <LegalLink href='https://www.linkedin.com/in/brian-ziani-7340a6154/' target='__blank' title='Link to Ziani Brian Linked In'> ZIANI Brian</LegalLink>
                    </LegalText>
                </LegalsWrapper>
                <LegalsWrapper>
                    <LegalTitle>Who is hosting this website ?</LegalTitle>
                    <LegalLink href='https://www.ionos.com/' target='__blank' title="Link to IONOS's website">IONOS</LegalLink>
                    1&1 IONOS Inc.<br/>
                    701 Lee Road, Suite 300<br/>
                    Chesterbrook<br/>
                    PA 19087<br/>
                    <br/>
                    IONOS Cloud Inc.<br/>
                    200 Continental Drive, Suite 401<br/>
                    Newark<br/>
                    DE 19713<br/>
                    <br/>
                    Telephone:<br/>
                    1&1 IONOS Inc.: 1-484-254-5555<br/>
                    IONOS Cloud Inc.: 1-267-481-7983<br/>
                    <br/>
                    Email:<br/>
                    1&1 IONOS Inc.: info@ionos.com<br/>
                    IONOS Cloud Inc.: info@cloud.ionos.com<br/>
                </LegalsWrapper>
            </Fade>
        </motion.div>
    )
}

export default Legals
