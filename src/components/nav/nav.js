import React, {useState , Suspense} from 'react';
import Navbar from './navbar';
import Sidebar from './sidebar';
import {BrowserRouter as Router, Switch, Route, useHistory, useLocation} from 'react-router-dom';
import Loading from './loading';
import Footer from '../footer/footer';
import { AnimatePresence } from "framer-motion";

const Home = React.lazy(() => import('../home'));
const Contact = React.lazy(() => import('../contact'));
const Download = React.lazy(() => import('../download'));
const About = React.lazy(() => import('../about'));
const Tutorials = React.lazy(() => import('../tutorials'));
const LegalNotices = React.lazy(() => import('../legals'));


const Nav = () => {
    const [isOpen, setOpen] = useState(false);
    const toggle = () => {
        setOpen(!isOpen)
    }
    const location = useLocation();

    return (
        <AnimatePresence exitBeforeEnter>
            <Suspense fallback={<Loading/>}>
                    <Sidebar isOpen={isOpen} toggle={toggle}/>
                    <Navbar toggle={toggle}/>
                    <Switch location={location} key={location.pathname}>
                        <Route exact path='/' render={() => <Home/>} />
                        <Route path='/contact' render={() => <Contact/>} />
                        <Route path='/download' render={() => <Download/>} />
                        <Route path='/about' render={() => <About/>} />
                        <Route path='/tutorials' render={() => <Tutorials/>} />
                        <Route path='/legals' render={() => <LegalNotices/>} />
                    </Switch>
                <Footer />
            </Suspense>
        </AnimatePresence>
    )
}

export default Nav;