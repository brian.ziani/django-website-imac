import React from 'react';
import {FaBars} from 'react-icons/fa'
import {Nav, 
    NavbarContainer, 
    Logo, 
    MobileIcon, 
    NavItem, 
    NavMenu, 
    NavLinks, 
    NavBtn,
    NavBtnLink,
    LogoImg
} from './navbarElements';
import img from '../../img/icon.png'

const Navbar = ({toggle}) => {
    return (
        <>
            <Nav>
                <NavbarContainer>
                    <Logo to='/'><LogoImg src={img}/></Logo>
                    <MobileIcon>
                        <FaBars onClick={toggle} />
                    </MobileIcon>
                    <NavMenu>
                        <NavItem>
                            <NavLinks to={'/'}>Home</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to={'/about'}>About</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to={'/contact'}>Contact</NavLinks>
                        </NavItem>
                    </NavMenu>
                    <NavBtn>
                            <NavBtnLink to={'/download'}>Download</NavBtnLink>
                    </NavBtn>
                </NavbarContainer>
            </Nav>
        </>
    )
}

export default Navbar
