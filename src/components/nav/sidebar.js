import React from 'react'
import {SidebarContainer, 
    CloseIcon, 
    Icon, 
    SideBarLink, 
    SideBarBtn, 
    SideBtnWrap, 
    SidebarWrapper,
    SidebarMenu
} from './sidebarElements'

const Sidebar = ({isOpen, toggle}) => {
    return (
        <>
            <SidebarContainer isOpen={isOpen} toggle={toggle}>
                <Icon onClick={toggle}>
                    <CloseIcon />
                </Icon>
                <SidebarWrapper>
                    <SidebarMenu>
                        <SideBarLink onClick={toggle} to={'/'}>Home</SideBarLink>
                        <SideBarLink onClick={toggle} to={'/about'}>About</SideBarLink>
                        <SideBarLink onClick={toggle} to={'/download'}>Download</SideBarLink>
                        <SideBarLink onClick={toggle} to={'/contact'}>Contact</SideBarLink>
                    </SidebarMenu>
                    <SideBtnWrap>
                        <SideBarBtn className='isNotDisplay' to={'/signin'}>Sign In</SideBarBtn>
                    </SideBtnWrap>
                </SidebarWrapper>
            </SidebarContainer>
        </>
    )
}

export default Sidebar
