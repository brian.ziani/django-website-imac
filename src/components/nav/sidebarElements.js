import styled from 'styled-components';
import {FaTimes} from 'react-icons/fa';
import {Link} from 'react-router-dom'

export const SidebarContainer = styled.aside`
    position:fixed;
    z-index:999;
    width:100%;
    height:100%;
    background:#fff;
    display: grid;
    align-items:center;
    top:0;
    left:0;
    transition:0.3s ease-in-out;
    opacity: ${({ isOpen })  => (isOpen ? '100%' : '0')};
    top : ${({ isOpen })  => (isOpen ? '0' : '-100%')};
`

export const CloseIcon = styled(FaTimes)`
    color:#fff;
`
export const Icon = styled.div`
        display: block;
        position: absolute;
        top:1.5rem;
        right: 1.5rem;
        background:transparent;
        transform:translate (-100% , 60%);
        font-size : 1.8rem;
        cursor: pointer;
        color:#000;
        outline:none;
`

export const SidebarWrapper = styled.div`
    color:#000;
`
export const SidebarMenu = styled.ul`
    display:grid;
    grid-template-columns: 1fr;
    grid-template-rows:repeat(6, 80px);
    text-align: center;

    @media screen and (max-width: 480px){
        grid-template-rows:repeat(6, 60px);
    }
`

export const SideBarLink = styled(Link)`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.5rem;
    text-decoration: none;
    list-style: none;
    transition: 0.3s ease-in-out;
    color:#000;
    cursor: pointer;

    &:hover{
        color:#F24C27;
        transition: 0.2s ease-in-out;
    }
`
export const SideBtnWrap = styled.div`
    display: flex;
    justify-content: center;
`

export const SideBarBtn = styled(Link)`
    border-radius: 50px;
    background: #000;
    white-space: nowrap;
    padding: 10px 22px;
    color:#000;
    font-size:16px;
    border:none;
    outline:none;
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    text-decoration: none;
`