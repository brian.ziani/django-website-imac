import React from 'react'
import { ContactCards, ContactCardTitle, ContactCardSvg, ContactCardP } from './contact/contactElements'
import Fade from 'react-reveal/Fade'
import {FaFacebook, FaInstagram, FaYoutube} from 'react-icons/fa'
import {SiTiktok} from 'react-icons/si'

const SocialMediaBanner = () => (
    <Fade top>
        <ContactCards height='300px' width="300px" href='https://www.facebook.com/django.vj' target='__blank'>
            <ContactCardSvg><FaFacebook/></ContactCardSvg>
            <ContactCardTitle>Facebook</ContactCardTitle>
            <ContactCardP textAlign='justify'>Follow us on Facebook to get the latest news !</ContactCardP>
        </ContactCards>
        <ContactCards height='300px' width="300px" href='https://www.instagram.com/django.vj' target='__blank'>
            <ContactCardSvg><FaInstagram/></ContactCardSvg>
            <ContactCardTitle>Instagram</ContactCardTitle>
            <ContactCardP textAlign='justify'>Follow us on Instagram to see our demos and take inspiration for your next creations !</ContactCardP>
        </ContactCards>
        <ContactCards height='300px' width="300px" href='https://www.youtube.com/channel/UCdGMTVCx1yfojoPKFGN2SDQ?fbclid=IwAR3UxYDM9PtZOs8xVSiXuSe9o1srduE4fo82ZVnVczu2_IXiaT0XUY8aV6Q' target='__blank'>
            <ContactCardSvg><FaYoutube/></ContactCardSvg>
            <ContactCardTitle>Youtube</ContactCardTitle>
            <ContactCardP textAlign='justify'>Watch our tutorials on Youtube to know more about Django and its features !</ContactCardP>
        </ContactCards>
        <ContactCards height='300px' width="300px" href='https://www.tiktok.com/@django.vj' target='__blank'>
            <ContactCardSvg><SiTiktok/></ContactCardSvg>
            <ContactCardTitle>Tiktok</ContactCardTitle>
            <ContactCardP textAlign='justify'>Follow us on TikTok to get inspired by our fancy videos !</ContactCardP>
    </ContactCards>
    </Fade>
)

export default SocialMediaBanner