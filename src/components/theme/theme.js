
import styled from 'styled-components'

export const Container = styled.div`
    max-width: 1100px;
    position:relative;
    align-self:center;
    margin:0 auto;
    margin-top:100px;
    @media screen and (max-width:420px){
        margin:0;
    }
`

export const theme = {
    colors: {
        gradient:{
            primary : ['#dc2430', '#7b4397'],
            secondary : ['#185a9d', '#43cea2'],
            hero : ['#2104D9', '#030A48']
        }
    },
};