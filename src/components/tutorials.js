import React from 'react'
import { ContactTitle, ContactTitleWrapper } from './contact/contactElements';
import { motion } from "framer-motion"
import { Fade } from '@material-ui/core';

const Tutorials = () => {
    return (
        <motion.div exit={{ opacity: 0 }}>
            <Fade top>
                <ContactTitleWrapper>
                    <ContactTitle>Tutorials</ContactTitle>
                </ContactTitleWrapper>

            </Fade>
        </motion.div>
    )
}

export default Tutorials