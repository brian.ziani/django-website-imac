const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const zlib = require("zlib");
const path = require('path');
const dotenv = require('dotenv').config({path: __dirname + '/.env'});

let config = {
    entry: path.resolve(__dirname, 'src/') + path.join('/index.js'),
    output: {
        path : path.resolve(__dirname, 'dist'),
        filename : '[name].[contenthash].js', 
    },
    devServer : {
        contentBase: path.join(__dirname),
        compress: true,
        host: '127.0.0.1',
        hot: true,
        port: 9000,
        publicPath: '/',
        historyApiFallback: true,
    },
    watch : true,
    module: {
        rules: [
          {
            test: /\.scss$/,
            use: ["style-loader", "css-loader", "sass-loader"]
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: ["babel-loader"]
          },
          {
            test: /\.(png|jpg|svg|gif)$/i,
            loader: 'file-loader',
            options: {
              name: '[name][contenthash].[ext]',
              outputPath: 'img',
            },
          },
          {
            test: /\.(woff|woff2|eot|ttf|otf)$/i,
            loader: 'file-loader',
            options: {
              name: '[name][contenthash].[ext]',
              outputPath: 'fonts',
            },
          },
          {
            test: /\.(zip)$/i,
            loader: 'file-loader',
            options: {
              name: '[name][contenthash].[ext]',
              outputPath: 'realease',
            },
          }
        ]
    },
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
          chunks: 'all',
          maxInitialRequests: Infinity,
          minSize: 0,
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name(module) {
                // get the name. E.g. node_modules/packageName/not/this/part.js
                // or node_modules/packageName
                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    
                // npm package names are URL-safe, but some servers don't like @ symbols
                return `npm.${packageName.replace('@', '')}`;
              },
            },
          },
        },
      },
    plugins: [
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, "index.html")
        }),
        new webpack.ids.HashedModuleIdsPlugin(),
        new webpack.DefinePlugin({
          "process.env": JSON.stringify(dotenv.parsed)
        }),
        new CompressionPlugin({
          filename: "[path][base].br",
          algorithm: "brotliCompress",
          test: /\.(js|css|html)$/,
          compressionOptions: {
            params: {
              [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
            },
          },
          threshold: 10240,
          minRatio: 0.8,
          deleteOriginalAssets: false,
        }),
        new ImageMinimizerPlugin({
          minimizerOptions: {
            // Lossless optimization with custom option
            // Feel free to experiment with options for better result for you
            plugins: [
              ['mozjpeg', { progressive: true }],
              ['pngquant', { optimizationLevel: 6 }],
              ['svgo', {plugins: [{removeViewBox: true,},],},],
            ],
          },
        }),
    ],
}

module.exports = config;